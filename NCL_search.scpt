FasdUAS 1.101.10   ��   ��    k             l      ��  ��   NHThe MIT License (MIT)

Copyright (c) [2015] [NCL_autocomplete abrammer]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
     � 	 	� T h e   M I T   L i c e n s e   ( M I T ) 
 
 C o p y r i g h t   ( c )   [ 2 0 1 5 ]   [ N C L _ a u t o c o m p l e t e   a b r a m m e r ] 
 
 P e r m i s s i o n   i s   h e r e b y   g r a n t e d ,   f r e e   o f   c h a r g e ,   t o   a n y   p e r s o n   o b t a i n i n g   a   c o p y 
 o f   t h i s   s o f t w a r e   a n d   a s s o c i a t e d   d o c u m e n t a t i o n   f i l e s   ( t h e   " S o f t w a r e " ) ,   t o   d e a l 
 i n   t h e   S o f t w a r e   w i t h o u t   r e s t r i c t i o n ,   i n c l u d i n g   w i t h o u t   l i m i t a t i o n   t h e   r i g h t s 
 t o   u s e ,   c o p y ,   m o d i f y ,   m e r g e ,   p u b l i s h ,   d i s t r i b u t e ,   s u b l i c e n s e ,   a n d / o r   s e l l 
 c o p i e s   o f   t h e   S o f t w a r e ,   a n d   t o   p e r m i t   p e r s o n s   t o   w h o m   t h e   S o f t w a r e   i s 
 f u r n i s h e d   t o   d o   s o ,   s u b j e c t   t o   t h e   f o l l o w i n g   c o n d i t i o n s : 
 
 T h e   a b o v e   c o p y r i g h t   n o t i c e   a n d   t h i s   p e r m i s s i o n   n o t i c e   s h a l l   b e   i n c l u d e d   i n   a l l 
 c o p i e s   o r   s u b s t a n t i a l   p o r t i o n s   o f   t h e   S o f t w a r e . 
 
 T H E   S O F T W A R E   I S   P R O V I D E D   " A S   I S " ,   W I T H O U T   W A R R A N T Y   O F   A N Y   K I N D ,   E X P R E S S   O R 
 I M P L I E D ,   I N C L U D I N G   B U T   N O T   L I M I T E D   T O   T H E   W A R R A N T I E S   O F   M E R C H A N T A B I L I T Y , 
 F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E   A N D   N O N I N F R I N G E M E N T .   I N   N O   E V E N T   S H A L L   T H E 
 A U T H O R S   O R   C O P Y R I G H T   H O L D E R S   B E   L I A B L E   F O R   A N Y   C L A I M ,   D A M A G E S   O R   O T H E R 
 L I A B I L I T Y ,   W H E T H E R   I N   A N   A C T I O N   O F   C O N T R A C T ,   T O R T   O R   O T H E R W I S E ,   A R I S I N G   F R O M , 
 O U T   O F   O R   I N   C O N N E C T I O N   W I T H   T H E   S O F T W A R E   O R   T H E   U S E   O R   O T H E R   D E A L I N G S   I N   T H E 
 S O F T W A R E . 
   
  
 l     ��������  ��  ��        l    � ����  O    �    O    �    k    �       r        l    ����  n        1    ��
�� 
pnam  4   �� 
�� 
docu  m    ���� ��  ��    o      ���� 0 filename        r        m         � ! !  .  n      " # " 1    ��
�� 
txdl # 1    ��
�� 
ascr   $ % $ r      & ' & n     ( ) ( 4   �� *
�� 
citm * m    ������ ) o    ���� 0 filename   ' o      ���� 0 	extension   %  + , + Z  ! - - .���� - >  ! $ / 0 / o   ! "���� 0 	extension   0 m   " # 1 1 � 2 2  n c l . L   ' )����  ��  ��   ,  3 4 3 r   . 3 5 6 5 m   . / 7 7 � 8 8   6 n      9 : 9 1   0 2��
�� 
txdl : 1   / 0��
�� 
ascr 4  ; < ; l  4 4�� = >��   = 2 , Get some character and cursor info to start    > � ? ? X   G e t   s o m e   c h a r a c t e r   a n d   c u r s o r   i n f o   t o   s t a r t <  @ A @ r   4 ; B C B n   4 9 D E D 1   7 9��
�� 
SLin E 1   4 7��
�� 
pusl C o      ���� 0 linenum lineNum A  F G F r   < N H I H \   < J J K J l  < A L���� L n   < A M N M 1   ? A��
�� 
Ofse N 1   < ?��
�� 
pusl��  ��   K l  A I O���� O n   A I P Q P 1   G I��
�� 
Ofse Q 4   A G�� R
�� 
clin R o   E F���� 0 linenum lineNum��  ��   I o      ���� 0 	cursorpos 	cursorPos G  S T S l  O O��������  ��  ��   T  U V U Z   O ] W X���� W =  O T Y Z Y o   O R���� 0 	cursorpos 	cursorPos Z m   R S����   X L   W Y����  ��  ��   V  [ \ [ r   ^ p ] ^ ] I  ^ l�� _��
�� .corecnte****       **** _ n   ^ h ` a ` 2  d h��
�� 
cha  a 4   ^ d�� b
�� 
clin b o   b c���� 0 linenum lineNum��   ^ o      ���� 0 	endofline   \  c d c l  q q��������  ��  ��   d  e f e r   q � g h g n   q ~ i j i 4   w ~�� k
�� 
cha  k o   z }���� 0 	cursorpos 	cursorPos j 4   q w�� l
�� 
clin l o   u v���� 0 linenum lineNum h o      ���� 0 curchar curChar f  m n m Z   � � o p���� o =  � � q r q c   � � s t s o   � ����� 0 curchar curChar t m   � ���
�� 
ctxt r m   � � u u � v v    p L   � �����  ��  ��   n  w x w l  � ���������  ��  ��   x  y z y r   � � { | { n   � � } ~ } 7  � ���  �
�� 
cha   m   � �����  � o   � ����� 0 	cursorpos 	cursorPos ~ 4   � ��� �
�� 
clin � o   � ����� 0 linenum lineNum | o      ���� 0 thewords theWords z  � � � r   � � � � � n   � � � � � 4  � ��� �
�� 
cwor � m   � ������� � o   � ����� 0 thewords theWords � o      ���� 0 theword theWord �  ��� � l  � ���������  ��  ��  ��    4   �� �
�� 
cwin � m    ����   m      � ��                                                                                  !Rch  alis    f  Macintosh HD               ���H+  ��TextWrangler.app                                               +���O�Y        ����  	                Applications    �1      �O��    ��  +Macintosh HD:Applications: TextWrangler.app   "  T e x t W r a n g l e r . a p p    M a c i n t o s h   H D  Applications/TextWrangler.app   / ��  ��  ��     � � � l     ��������  ��  ��   �  ��� � l  � � ����� � O   � � � � � k   � � � �  � � � I  � �������
�� .miscactvnull��� ��� null��  ��   �  � � � r   � � � � � I  � ����� �
�� .corecrel****      � null��   � �� ���
�� 
kocl � m   � ���
�� 
cwin��   � o      ���� 0 mytab myTab �  � � � r   � � � � � b   � � � � � m   � � � � � � � � h t t p : / / w w w . g o o g l e . c o m / s e a r c h ? b t n I = I % 2 7 m + F e e l i n g + L u c k y & i e = U T F - 8 & o e = U T F - 8 & q = n c l + � o   � ����� 0 theword theWord � o      ���� 0 myurl myURL �  ��� � r   � � � � � o   � ����� 0 myurl myURL � n       � � � 1   � ���
�� 
URL  � n   � � � � � 4  � ��� �
�� 
CrTb � m   � �����  � o   � ����� 0 mytab myTab��   � m   � � � ��                                                                                  rimZ  alis    h  Macintosh HD               ���H+  ��Google Chrome.app                                              /�Ј7�        ����  	                Applications    �1      Ј}�    ��  ,Macintosh HD:Applications: Google Chrome.app  $  G o o g l e   C h r o m e . a p p    M a c i n t o s h   H D  Applications/Google Chrome.app  / ��  ��  ��  ��       �� � � � ������� � � � � �������������   � ������������������������~�}�|�{
�� .aevtoappnull  �   � ****�� 0 filename  �� 0 	extension  �� 0 linenum lineNum�� 0 	cursorpos 	cursorPos�� 0 	endofline  �� 0 curchar curChar�� 0 thewords theWords�� 0 theword theWord�� 0 mytab myTab�� 0 myurl myURL�  �~  �}  �|  �{   � �z ��y�x � ��w
�z .aevtoappnull  �   � **** � k     � � �   � �  ��v�v  �y  �x   �   � $ ��u�t�s�r  �q�p�o�n 1 7�m�l�k�j�i�h�g�f�e�d�c u�b�a�` ��_�^�]�\ ��[�Z�Y
�u 
cwin
�t 
docu
�s 
pnam�r 0 filename  
�q 
ascr
�p 
txdl
�o 
citm�n 0 	extension  
�m 
pusl
�l 
SLin�k 0 linenum lineNum
�j 
Ofse
�i 
clin�h 0 	cursorpos 	cursorPos
�g 
cha 
�f .corecnte****       ****�e 0 	endofline  �d 0 curchar curChar
�c 
ctxt�b 0 thewords theWords
�a 
cwor�` 0 theword theWord
�_ .miscactvnull��� ��� null
�^ 
kocl
�] .corecrel****      � null�\ 0 mytab myTab�[ 0 myurl myURL
�Z 
CrTb
�Y 
URL �w �� �*�k/ �*�k/�,E�O���,FO��i/E�O�� hY hO���,FO*�,�,E�O*�,�,*a �/�,E` O_ j  hY hO*a �/a -j E` O*a �/a _ /E` O_ a &a   hY hO*a �/[a \[Zk\Z_ 2E` O_ a i/E` OPUUOa  2*j O*a �l E` Oa  _ %E` !O_ !_ a "k/a #,FU � � � � $ h o v _ c o m p a r i s o n . n c l � � � �  n c l�� R�� �� ! �  � �  ��X�W �  ��V�U
�V 
TxtD�U 
�X 
cha �W	� �  � �  ��T�S�R �  ��Q�P
�Q 
TxtD�P 
�T 
cha �S	��R	� �  � �  ��O�N�M �  ��L�K
�L 
TxtD�K 
�O 
cha �N	��M	� �  � �  ��J�I�H
�J 
cwin�I�
�H kfrmID   � � � � � h t t p : / / w w w . g o o g l e . c o m / s e a r c h ? b t n I = I % 2 7 m + F e e l i n g + L u c k y & i e = U T F - 8 & o e = U T F - 8 & q = n c l + r u n a v e _ n��  ��  ��  ��  ��   ascr  ��ޭ