FasdUAS 1.101.10   ��   ��    k             l      ��  ��   NHThe MIT License (MIT)

Copyright (c) [2015] [NCL_autocomplete abrammer]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
     � 	 	� T h e   M I T   L i c e n s e   ( M I T ) 
 
 C o p y r i g h t   ( c )   [ 2 0 1 5 ]   [ N C L _ a u t o c o m p l e t e   a b r a m m e r ] 
 
 P e r m i s s i o n   i s   h e r e b y   g r a n t e d ,   f r e e   o f   c h a r g e ,   t o   a n y   p e r s o n   o b t a i n i n g   a   c o p y 
 o f   t h i s   s o f t w a r e   a n d   a s s o c i a t e d   d o c u m e n t a t i o n   f i l e s   ( t h e   " S o f t w a r e " ) ,   t o   d e a l 
 i n   t h e   S o f t w a r e   w i t h o u t   r e s t r i c t i o n ,   i n c l u d i n g   w i t h o u t   l i m i t a t i o n   t h e   r i g h t s 
 t o   u s e ,   c o p y ,   m o d i f y ,   m e r g e ,   p u b l i s h ,   d i s t r i b u t e ,   s u b l i c e n s e ,   a n d / o r   s e l l 
 c o p i e s   o f   t h e   S o f t w a r e ,   a n d   t o   p e r m i t   p e r s o n s   t o   w h o m   t h e   S o f t w a r e   i s 
 f u r n i s h e d   t o   d o   s o ,   s u b j e c t   t o   t h e   f o l l o w i n g   c o n d i t i o n s : 
 
 T h e   a b o v e   c o p y r i g h t   n o t i c e   a n d   t h i s   p e r m i s s i o n   n o t i c e   s h a l l   b e   i n c l u d e d   i n   a l l 
 c o p i e s   o r   s u b s t a n t i a l   p o r t i o n s   o f   t h e   S o f t w a r e . 
 
 T H E   S O F T W A R E   I S   P R O V I D E D   " A S   I S " ,   W I T H O U T   W A R R A N T Y   O F   A N Y   K I N D ,   E X P R E S S   O R 
 I M P L I E D ,   I N C L U D I N G   B U T   N O T   L I M I T E D   T O   T H E   W A R R A N T I E S   O F   M E R C H A N T A B I L I T Y , 
 F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E   A N D   N O N I N F R I N G E M E N T .   I N   N O   E V E N T   S H A L L   T H E 
 A U T H O R S   O R   C O P Y R I G H T   H O L D E R S   B E   L I A B L E   F O R   A N Y   C L A I M ,   D A M A G E S   O R   O T H E R 
 L I A B I L I T Y ,   W H E T H E R   I N   A N   A C T I O N   O F   C O N T R A C T ,   T O R T   O R   O T H E R W I S E ,   A R I S I N G   F R O M , 
 O U T   O F   O R   I N   C O N N E C T I O N   W I T H   T H E   S O F T W A R E   O R   T H E   U S E   O R   O T H E R   D E A L I N G S   I N   T H E 
 S O F T W A R E . 
   
  
 l     ��������  ��  ��     ��  l   � ����  O   �    O   �    k   �       r        l    ����  n        1    ��
�� 
pnam  4   �� 
�� 
docu  m    ���� ��  ��    o      ���� 0 filename        r        m       �      .  n      ! " ! 1    ��
�� 
txdl " 1    ��
�� 
ascr   # $ # r      % & % n     ' ( ' 4   �� )
�� 
citm ) m    ������ ( o    ���� 0 filename   & o      ���� 0 	extension   $  * + * Z  ! - , -���� , >  ! $ . / . o   ! "���� 0 	extension   / m   " # 0 0 � 1 1  n c l - L   ' )����  ��  ��   +  2 3 2 r   . 3 4 5 4 m   . / 6 6 � 7 7   5 n      8 9 8 1   0 2��
�� 
txdl 9 1   / 0��
�� 
ascr 3  : ; : l  4 4�� < =��   < 2 , Get some character and cursor info to start    = � > > X   G e t   s o m e   c h a r a c t e r   a n d   c u r s o r   i n f o   t o   s t a r t ;  ? @ ? r   4 ; A B A n   4 9 C D C 1   7 9��
�� 
SLin D 1   4 7��
�� 
pusl B o      ���� 0 linenum lineNum @  E F E r   < N G H G \   < J I J I l  < A K���� K n   < A L M L 1   ? A��
�� 
Ofse M 1   < ?��
�� 
pusl��  ��   J l  A I N���� N n   A I O P O 1   G I��
�� 
Ofse P 4   A G�� Q
�� 
clin Q o   E F���� 0 linenum lineNum��  ��   H o      ���� 0 	cursorpos 	cursorPos F  R S R l  O O��������  ��  ��   S  T U T Z   O ] V W���� V =  O T X Y X o   O R���� 0 	cursorpos 	cursorPos Y m   R S����   W L   W Y����  ��  ��   U  Z [ Z r   ^ p \ ] \ I  ^ l�� ^��
�� .corecnte****       **** ^ n   ^ h _ ` _ 2  d h��
�� 
cha  ` 4   ^ d�� a
�� 
clin a o   b c���� 0 linenum lineNum��   ] o      ���� 0 	endofline   [  b c b l  q q��������  ��  ��   c  d e d r   q � f g f n   q ~ h i h 4   w ~�� j
�� 
cha  j o   z }���� 0 	cursorpos 	cursorPos i 4   q w�� k
�� 
clin k o   u v���� 0 linenum lineNum g o      ���� 0 curchar curChar e  l m l Z   � � n o���� n =  � � p q p c   � � r s r o   � ����� 0 curchar curChar s m   � ���
�� 
ctxt q m   � � t t � u u    o L   � �����  ��  ��   m  v w v l  � ���������  ��  ��   w  x y x r   � � z { z n   � � | } | 7  � ��� ~ 
�� 
cha  ~ m   � �����   o   � ����� 0 	cursorpos 	cursorPos } 4   � ��� �
�� 
clin � o   � ����� 0 linenum lineNum { o      ���� 0 thewords theWords y  � � � r   � � � � � n   � � � � � 4  � ��� �
�� 
cwor � m   � ������� � o   � ����� 0 thewords theWords � o      ���� 0 theword theWord �  � � � l  � ���������  ��  ��   �  � � � r   � � � � � I  � ��� ���
�� .sysoexecTEXT���     TEXT � b   � � � � � b   � � � � � m   � � � � � � � � s e d   ' s / < [ ^ > ] * > / / g '   ~ / L i b r a r y / A p p l i c a t i o n \   S u p p o r t / T e x t W r a n g l e r / L a n g u a g e \   M o d u l e s / N C L . p l i s t   |   g r e p   ' ^ � o   � ����� 0 theword theWord � m   � � � � � � �  '  ��   � o      ���� 	0 texts   �  � � � Z   � � � ����� � =  � � � � � c   � � � � � o   � ����� 	0 texts   � m   � ���
�� 
ctxt � m   � � � � � � �   � L   � �����  ��  ��   �  � � � r   � � � � � J   � �����   � o      ���� $0 possiblecomplete possibleComplete �  � � � X   � ��� � � s   � � � o  	���� 0 w   � n       � � �  ;   � o  	���� $0 possiblecomplete possibleComplete�� 0 w   � n  � � � � � 2  � ���
�� 
cwor � o   � ����� 	0 texts   �  � � � Z  G � ��� � � =  � � � n   � � � 1  ��
�� 
leng � o  ���� $0 possiblecomplete possibleComplete � m  ����  � r   , � � � n   ( � � � 4 #(�� �
�� 
cobj � m  &'����  � o   #���� $0 possiblecomplete possibleComplete � o      ���� 0 newword newWord��   � r  /G � � � I /C�� � �
�� .gtqpchltns    @   @ ns   � o  /2���� $0 possiblecomplete possibleComplete � �� ���
�� 
inSL � J  5? � �  ��� � n  5= � � � 4 8=�� �
�� 
cwor � m  ;<����  � o  58���� 	0 texts  ��  ��   � o      ���� 0 newword newWord �  � � � Z  HV � ����� � = HM � � � o  HK���� 0 newword newWord � m  KL��
�� boovfals � L  PR����  ��  ��   �  � � � l WW��������  ��  ��   �  � � � l WW��������  ��  ��   �  � � � r  Wq � � � n  Wm � � � 7 ]m�� � �
�� 
cha  � o  cg�� 0 	cursorpos 	cursorPos � o  hl�~�~ 0 	endofline   � 4  W]�} �
�} 
clin � o  [\�|�| 0 linenum lineNum � o      �{�{ 0 	nextwords 	nextWords �  � � � r  r� � � � \  r� � � � l rw ��z�y � n  rw � � � 1  uw�x
�x 
Ofse � 1  ru�w
�w 
pusl�z  �y   � l w� ��v�u � n  w� � � � 1  ~��t
�t 
leng � l w~ ��s�r � c  w~ � � � o  wz�q�q 0 theword theWord � m  z}�p
�p 
TEXT�s  �r  �v  �u   � o      �o�o 0 startpos startPos �  � � � r  �� � � � l �� ��n�m � \  �� � � � l �� ��l�k � [  �� � � � l �� ��j�i � n  �� � � � 1  ���h
�h 
Ofse � 1  ���g
�g 
pusl�j  �i   � l �� ��f�e � n  �� � � � 1  ���d
�d 
leng � n  �� � � � 4 ���c �
�c 
cwor � m  ���b�b  � o  ���a�a 0 	nextwords 	nextWords�f  �e  �l  �k   � m  ���`�` �n  �m   � o      �_�_ 0 endpos endPos �  � � � Z �� � ��^�] � A  ��   o  ���\�\ 0 endpos endPos o  ���[�[ 0 startpos startPos � r  �� o  ���Z�Z 0 startpos startPos o      �Y�Y 0 endpos endPos�^  �]   �  r  �� o  ���X�X 0 newword newWord n      	 7 ���W

�W 
cha 
 o  ���V�V 0 startpos startPos o  ���U�U 0 endpos endPos	 4  ���T
�T 
docu m  ���S�S   I ���R�Q
�R .miscslctnull��� ��� obj  n  �� n  �� 9  ���P
�P 
cins 4  ���O
�O 
cha  l ���N�M \  �� [  �� o  ���L�L 0 startpos startPos l ���K�J n  �� 1  ���I
�I 
leng l ���H�G c  �� o  ���F�F 0 newword newWord m  ���E
�E 
TEXT�H  �G  �K  �J   m  ���D�D �N  �M   4  ���C 
�C 
docu  m  ���B�B �Q   !"! l ���A�@�?�A  �@  �?  " #$# l ���>�=�<�>  �=  �<  $ %�;% l ���:�9�8�:  �9  �8  �;    4   �7&
�7 
cwin& m    �6�6   m     ''�                                                                                  !Rch  alis    f  Macintosh HD               ���H+  ��TextWrangler.app                                               +���O�Y        ����  	                Applications    �1      �O��    ��  +Macintosh HD:Applications: TextWrangler.app   "  T e x t W r a n g l e r . a p p    M a c i n t o s h   H D  Applications/TextWrangler.app   / ��  ��  ��  ��       �5()�5  ( �4
�4 .aevtoappnull  �   � ****) �3*�2�1+,�0
�3 .aevtoappnull  �   � ***** k    �--  �/�/  �2  �1  + �.�. 0 w  , -'�-�,�+�* �)�(�'�& 0 6�%�$�#�"�!� ����� t��� � ��� �������������
�	
�- 
cwin
�, 
docu
�+ 
pnam�* 0 filename  
�) 
ascr
�( 
txdl
�' 
citm�& 0 	extension  
�% 
pusl
�$ 
SLin�# 0 linenum lineNum
�" 
Ofse
�! 
clin�  0 	cursorpos 	cursorPos
� 
cha 
� .corecnte****       ****� 0 	endofline  � 0 curchar curChar
� 
ctxt� 0 thewords theWords
� 
cwor� 0 theword theWord
� .sysoexecTEXT���     TEXT� 	0 texts  � $0 possiblecomplete possibleComplete
� 
kocl
� 
cobj
� 
leng� 0 newword newWord
� 
inSL
� .gtqpchltns    @   @ ns  � 0 	nextwords 	nextWords
� 
TEXT� 0 startpos startPos� 0 endpos endPos
�
 
cins
�	 .miscslctnull��� ��� obj �0���*�k/�*�k/�,E�O���,FO��i/E�O�� hY hO���,FO*�,�,E�O*�,�,*a �/�,E` O_ j  hY hO*a �/a -j E` O*a �/a _ /E` O_ a &a   hY hO*a �/[a \[Zk\Z_ 2E` O_ a i/E` Oa _ %a %j E` O_ a &a   hY hOjvE`  O $_ a -[a !a "l kh  �_  6G[OY��O_  a #,k  _  a "k/E` $Y _  a %_ a k/kvl &E` $O_ $f  hY hO*a �/[a \[Z_ \Z_ 2E` 'O*�,�,_ a (&a #,E` )O*�,�,_ 'a k/a #,lE` *O_ *_ ) _ )E` *Y hO_ $*�k/[a \[Z_ )\Z_ *2FO*�k/a _ )_ $a (&a #,k/a +4j ,OPUUascr  ��ޭ