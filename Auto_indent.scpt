FasdUAS 1.101.10   ��   ��    k             l      ��  ��   NHThe MIT License (MIT)

Copyright (c) [2015] [NCL_autocomplete abrammer]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
     � 	 	� T h e   M I T   L i c e n s e   ( M I T ) 
 
 C o p y r i g h t   ( c )   [ 2 0 1 5 ]   [ N C L _ a u t o c o m p l e t e   a b r a m m e r ] 
 
 P e r m i s s i o n   i s   h e r e b y   g r a n t e d ,   f r e e   o f   c h a r g e ,   t o   a n y   p e r s o n   o b t a i n i n g   a   c o p y 
 o f   t h i s   s o f t w a r e   a n d   a s s o c i a t e d   d o c u m e n t a t i o n   f i l e s   ( t h e   " S o f t w a r e " ) ,   t o   d e a l 
 i n   t h e   S o f t w a r e   w i t h o u t   r e s t r i c t i o n ,   i n c l u d i n g   w i t h o u t   l i m i t a t i o n   t h e   r i g h t s 
 t o   u s e ,   c o p y ,   m o d i f y ,   m e r g e ,   p u b l i s h ,   d i s t r i b u t e ,   s u b l i c e n s e ,   a n d / o r   s e l l 
 c o p i e s   o f   t h e   S o f t w a r e ,   a n d   t o   p e r m i t   p e r s o n s   t o   w h o m   t h e   S o f t w a r e   i s 
 f u r n i s h e d   t o   d o   s o ,   s u b j e c t   t o   t h e   f o l l o w i n g   c o n d i t i o n s : 
 
 T h e   a b o v e   c o p y r i g h t   n o t i c e   a n d   t h i s   p e r m i s s i o n   n o t i c e   s h a l l   b e   i n c l u d e d   i n   a l l 
 c o p i e s   o r   s u b s t a n t i a l   p o r t i o n s   o f   t h e   S o f t w a r e . 
 
 T H E   S O F T W A R E   I S   P R O V I D E D   " A S   I S " ,   W I T H O U T   W A R R A N T Y   O F   A N Y   K I N D ,   E X P R E S S   O R 
 I M P L I E D ,   I N C L U D I N G   B U T   N O T   L I M I T E D   T O   T H E   W A R R A N T I E S   O F   M E R C H A N T A B I L I T Y , 
 F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E   A N D   N O N I N F R I N G E M E N T .   I N   N O   E V E N T   S H A L L   T H E 
 A U T H O R S   O R   C O P Y R I G H T   H O L D E R S   B E   L I A B L E   F O R   A N Y   C L A I M ,   D A M A G E S   O R   O T H E R 
 L I A B I L I T Y ,   W H E T H E R   I N   A N   A C T I O N   O F   C O N T R A C T ,   T O R T   O R   O T H E R W I S E ,   A R I S I N G   F R O M , 
 O U T   O F   O R   I N   C O N N E C T I O N   W I T H   T H E   S O F T W A R E   O R   T H E   U S E   O R   O T H E R   D E A L I N G S   I N   T H E 
 S O F T W A R E . 
   
  
 l     ��������  ��  ��        l    � ����  O    �    O    �    k    �       l    ��  ��    � �	set filename to the name of front document	set AppleScript's text item delimiters to "."	set extension to last text item of filename	if extension is not "ncl" then return     �  \ 	 s e t   f i l e n a m e   t o   t h e   n a m e   o f   f r o n t   d o c u m e n t  	 s e t   A p p l e S c r i p t ' s   t e x t   i t e m   d e l i m i t e r s   t o   " . "  	 s e t   e x t e n s i o n   t o   l a s t   t e x t   i t e m   o f   f i l e n a m e  	 i f   e x t e n s i o n   i s   n o t   " n c l "   t h e n   r e t u r n      l   ��  ��    2 , Get some character and cursor info to start     �   X   G e t   s o m e   c h a r a c t e r   a n d   c u r s o r   i n f o   t o   s t a r t      l    ��   !��     $ set selectionText to selection    ! � " " < s e t   s e l e c t i o n T e x t   t o   s e l e c t i o n   # $ # r     % & % m    ����   & o      ���� 0 indent_multiplier   $  ' ( ' l   ��������  ��  ��   (  )�� ) X    � *�� + * k   $ � , ,  - . - r   $ ) / 0 / c   $ ' 1 2 1 o   $ %���� 0 l   2 m   % &��
�� 
ctxt 0 o      ���� 0 thisline   .  3 4 3 r   * 2 5 6 5 n  * 0 7 8 7 I   + 0�� 9���� 0 trim   9  :�� : o   + ,���� 0 thisline  ��  ��   8  f   * + 6 o      ���� 0 thisline   4  ; < ; r   3 8 = > = o   3 4���� 0 thisline   > n       ? @ ? m   5 7��
�� 
ctxt @ o   4 5���� 0 l   <  A B A l  9 9��������  ��  ��   B  C�� C Z   9 � D E�� F D =  9 @ G H G l  9 > I���� I >  9 > J K J c   9 < L M L o   9 :���� 0 l   M m   : ;��
�� 
TEXT K m   < = N N � O O  ��  ��   H m   > ?��
�� boovtrue E k   C � P P  Q R Q Z   C Y S T���� S E  C M U V U J   C F W W  X�� X m   C D Y Y � Z Z  e n d��   V l  F L [���� [ c   F L \ ] \ n   F J ^ _ ^ 4  G J�� `
�� 
cwor ` m   H I����  _ o   F G���� 0 l   ] m   J K��
�� 
TEXT��  ��   T r   P U a b a \   P S c d c o   P Q���� 0 indent_multiplier   d m   Q R����  b o      ���� 0 indent_multiplier  ��  ��   R  e f e r   Z i g h g n  Z ` i j i I   [ `�� k���� 0 indenter   k  l�� l o   [ \���� 0 indent_multiplier  ��  ��   j  f   Z [ h n       m n m 8   d h��
�� 
insl n n   ` d o p o 4   a d�� q
�� 
cwor q m   b c����  p o   ` a���� 0 l   f  r�� r Z   j � s t���� s E  j y u v u J   j r w w  x y x m   j m z z � { {  d o y  |�� | m   m p } } � ~ ~  i f��   v l  r x ����  c   r x � � � n   r v � � � 4  s v�� �
�� 
cwor � m   t u����  � o   r s���� 0 l   � m   v w��
�� 
TEXT��  ��   t r   | � � � � [   |  � � � o   | }���� 0 indent_multiplier   � m   } ~����  � o      ���� 0 indent_multiplier  ��  ��  ��  ��   F r   � � � � � n  � � � � � I   � ��� ����� 0 indenter   �  ��� � o   � ����� 0 indent_multiplier  ��  ��   �  f   � � � o      ���� 0 l  ��  �� 0 l   + n    � � � 2   ��
�� 
clin � 4    �� �
�� 
docu � m    ���� ��    4   �� �
�� 
cwin � m    ����   m      � ��                                                                                  !Rch  alis    f  Macintosh HD               ���H+  ��TextWrangler.app                                               +���O�Y        ����  	                Applications    �1      �O��    ��  +Macintosh HD:Applications: TextWrangler.app   "  T e x t W r a n g l e r . a p p    M a c i n t o s h   H D  Applications/TextWrangler.app   / ��  ��  ��     � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i      � � � I      �� ����� 0 trim   �  ��� � o      ���� 0 sometext someText��  ��   � k     : � �  � � � W      � � � r   	  � � � n   	  � � � 7  
 �� � �
�� 
ctxt � m    ����  � m    ������ � o   	 
���� 0 sometext someText � o      ���� 0 sometext someText � H     � � C     � � � o    ���� 0 sometext someText � m     � � � � �    �  � � � l   ��������  ��  ��   �  � � � W    7 � � � r   % 2 � � � n   % 0 � � � 7  & 0�� � �
�� 
ctxt � m   * ,����  � m   - /������ � o   % &���� 0 sometext someText � o      ���� 0 sometext someText � H     $ � � D     # � � � o     !���� 0 sometext someText � m   ! " � � � � �    �  ��� � L   8 : � � o   8 9���� 0 sometext someText��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i     � � � I      �� ����� 0 indenter   �  ��� � o      ���� 0 
multiplier  ��  ��   � k      � �  � � � r      � � � m      � � � � �   � o      ���� 0 retword retWord �  � � � Y     ��� � ��� � r     � � � b     � � � o    ���� 0 retword retWord � m     � � � � �      � o      �� 0 retword retWord�� 0 r   � m    �~�~  � o    	�}�} 0 
multiplier  ��   �  ��| � L     � � o    �{�{ 0 retword retWord�|   �  � � � l     �z�y�x�z  �y  �x   �  ��w � l     �v�u�t�v  �u  �t  �w       �s � � � ��r ��q�s   � �p�o�n�m�l�k�p 0 trim  �o 0 indenter  
�n .aevtoappnull  �   � ****�m 0 indent_multiplier  �l 0 thisline  �k   � �j ��i�h � ��g�j 0 trim  �i �f ��f  �  �e�e 0 sometext someText�h   � �d�d 0 sometext someText �  ��c ��b
�c 
ctxt�b���g ; h���[�\[Zl\Zi2E�[OY��O h���[�\[Zk\Z�2E�[OY��O� � �a ��`�_ � ��^�a 0 indenter  �` �] ��]  �  �\�\ 0 
multiplier  �_   � �[�Z�Y�[ 0 
multiplier  �Z 0 retword retWord�Y 0 r   �  � ��^ �E�O k�kh ��%E�[OY��O� � �X ��W�V � ��U
�X .aevtoappnull  �   � **** � k     � � �  �T�T  �W  �V   � �S�S 0 l   �  ��R�Q�P�O�N�M�L�K�J�I�H N Y�G�F�E z }
�R 
cwin�Q 0 indent_multiplier  
�P 
docu
�O 
clin
�N 
kocl
�M 
cobj
�L .corecnte****       ****
�K 
ctxt�J 0 thisline  �I 0 trim  
�H 
TEXT
�G 
cwor�F 0 indenter  
�E 
insl�U �� �*�k/ �jE�O �*�k/�-[��l kh  ��&E�O)�k+ 
E�Oɠ�-FO��&�e  G�kv��k/�& 
�kE�Y hO)�k+ ��k/a 3FOa a lv��k/�& 
�kE�Y hY 
)�k+ E�[OY��UU�r   � � � �  �q  ascr  ��ޭ